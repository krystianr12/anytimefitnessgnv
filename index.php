<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Sign up at Anytime Fitness for only $1</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/css/normalize.css">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/main.min.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136245788-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-136245788-1');
    </script>

  </head>
  <body>

    <div class="container">
      <div class="logo-container">
        <img class="logo" src="/assets/images/logo.png" alt="Anytime Fitness Logo">
      </div>

      <div class="content-container">
        <h1 class="title">Sign up today for only <span id="highlight">$1 down</span> only through the month of March.</h1>
        <div class="list-container">
          <ul class="list">
            <li>24 Hour Access</li>
            <li>Worldwide Gym Access</li>
            <li>Nutritional Resources</li>
          </ul>
          <ul class="list">
            <li>Personal Training</li>
            <li>Group Classes</li>
            <li>Vitamins & Supplements</li>
          </ul>
        </div>
      </div>

      <div class="form-container">
        <p class="asterisk">* Offer ends March 31st, 2019.</p>
        <div class="form">
          <a class="submit-btn" href="https://www.anytimefitness.com/membership-inquiry/?club=1114" class="button">Sign up</a>
          <p class="security"><i class="fas fa-lock"></i> Your information is secured with encryption.</p>
        </div>

      </div>
    </div>


  </body>
</html>
