<p>Live a better, healthier life! Fill out the following information and we’ll contact you with the next step to complete your $1 membership sign up. </p>

<form class="contact-form" action="signup.php" method="post">
  <label for="firstname">First name</label>
  <input type="text" name="firstname">
  <label for="lastname">Last name</label>
  <input type="text" name="lastname">
  <label for="email">Email</label>
  <input type="text" name="email">
  <span class="promo"><input type="checkbox" class="checkbox" name="checkbox" value="Yes" checked>Send me information about promotions, news and events</span>
  <input type="submit" name="submit">
</form>
