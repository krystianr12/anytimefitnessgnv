<div class="container">
  <div class="ad">

    <div class="wrapper">
      <h1>Sign up for only $1 at Anytime Fitness!*</h1>
      <p>Get access to all the amazing features we have to offer you today. *Get your first month on Anytime Fitness for only $1.</p>

      <ul class="list">
        <li>24/7 access</li>
        <li>Fitness Consultation</li>
        <li>Team Workouts</li>
        <li>Personal Training</li>
        <li>And Much More!</li>
      </ul>

      <p class="notice">*Offer valid through January 31, 2019.</p>
    </div>

    <!-- STARTS FOOTER  -->
    <div class="footer">
      <div class="callout">
        <h6>More Info</h6>
        <p>For any additional information give us a call at: (352) 338-7722</p>
      </div>

      <div class="callout">
        <h6>Our Location</h6>
        <p>7070 SW Archer Rd, Gainesville, FL 32608</p>
      </div>
    </div>
  </div>

  <div class="login-form">
    <img class="logo" src="/assets/images/anytime-fitness-logo.png" alt="Anytime Fitness Logo">
    <?php

      if (isset($result) && $result == "success"){

        /** CHECK FOR TOKEN HERE TO PREVENT ATTACKS! **/

        echo "<p>Thank you for reaching out to us! We'll get back in touch with you as soon as possible.<br><br>Anytime Fitness Management.</p>";
      }

      else if (isset($result) && $result == "error"){
        echo "<p>Something went wrong. Please try again later.</p>";
        header("refresh:5; url=/signup.php");
      }

      else {
        include($_SERVER['DOCUMENT_ROOT'] . '/include/mailchimp-form.php');
        return;
      }


    ?>
  </div>
</div>
